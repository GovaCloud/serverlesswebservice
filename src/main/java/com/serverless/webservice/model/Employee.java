package com.serverless.webservice.model;

public class Employee {
	public static final Object EMPLOYEE_ID = "ID";
	public static final Object EMPLOYEE_NAME = "Name";
	private Long employeid;
	private String employeeName;
	public Long getEmployeid() {
		return employeid;
	}
	public void setEmployeid(Long employeid) {
		this.employeid = employeid;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	
	
}
