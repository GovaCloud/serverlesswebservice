package com.serverless.webservice.function;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.serverless.webservice.model.Employee;
import com.serverless.webservice.model.Schedule;
import com.serverless.webservice.model.ServerlessInput;
import com.serverless.webservice.model.ServerlessOutput;
import com.serverless.webservice.service.ScheduleService;

/**
 * Lambda function that triggered by the API Gateway event "GET /". It Queries DynamoDb and generate schedule and 
 * returns the content as the payload of the HTTP Response.
 */
public class GetSchedule implements RequestHandler<ServerlessInput, ServerlessOutput> {
    // DynamoDB table name
    private static final String TABLE_NAME = "EMPLOYEE";
    @Override
    public ServerlessOutput handleRequest(ServerlessInput serverlessInput, Context context) {
        
        AmazonDynamoDB dynamoDb = AmazonDynamoDBClientBuilder.standard().build();
        ServerlessOutput output = new ServerlessOutput();
        
        try {
          
            ArrayList<Employee> lstEmp = new ArrayList<Employee>();
            ScanRequest scanRequest = new ScanRequest().withTableName(TABLE_NAME);
            ScanResult result = null;
            
            do {

                if (result != null) {
                    scanRequest.setExclusiveStartKey(result.getLastEvaluatedKey());
                }
                result = dynamoDb.scan(scanRequest);
                List<Map<String, AttributeValue>> rows = result.getItems();

                // Iterate through All rows
                for (Map<String, AttributeValue> mapEmployeeRecord : rows) {
                    Employee employee = employeeParser(mapEmployeeRecord);
                    lstEmp.add(employee);
                }

            } while (result.getLastEvaluatedKey() != null);
            
            ScheduleService service = new ScheduleService();
            ArrayList<Schedule> lstSch = service.getSchedule(lstEmp);
            
            //Convert the java object to JSON 
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
            
            String arrayToJson = objectMapper.writeValueAsString(lstSch);
            
            output.setStatusCode(200);
            output.setBody(arrayToJson);
        } catch (Exception e) {
            output.setStatusCode(500);
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            output.setBody(sw.toString());
        }

        return output;
    }
    
    /* * Converts Map of Attribute values to Employee Domain Object */
    private Employee employeeParser(Map<String, AttributeValue> mapEmployeeRecord) {

        Employee employee = new Employee();

        try {

            AttributeValue employeeIdAttrValue = mapEmployeeRecord.get(Employee.EMPLOYEE_ID);
            AttributeValue NameAttrValue = mapEmployeeRecord.get(Employee.EMPLOYEE_NAME);

            employee.setEmployeid(Long.valueOf(employeeIdAttrValue.getN()));
            if (NameAttrValue != null) {
            	employee.setEmployeeName(mapEmployeeRecord.get(Employee.EMPLOYEE_NAME).getS());
            }

        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
        }

        return employee;
    }
}