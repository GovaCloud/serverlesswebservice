package com.serverless.webservice.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.serverless.webservice.model.Employee;
import com.serverless.webservice.model.Schedule;

public class ScheduleService {
	
	public ArrayList<Schedule> getSchedule(ArrayList<Employee> lstEmp){

	Map<String,Integer> map=new HashMap<>();
	ArrayList<Schedule> lstSch = new ArrayList<Schedule>();
	
	for(int i = 0; i<lstEmp.size(); i++){
		Employee emp = lstEmp.get(i);
		map.put(emp.getEmployeeName(), 2);
	}
	
	Calendar cal=Calendar.getInstance();
	for(int j=1;j<11;j++){
		SimpleDateFormat sdf=new SimpleDateFormat("dd-MMM-YYYY");
		cal.add(Calendar.DAY_OF_MONTH, 1);
		String tempDate=sdf.format(cal.getTime());
		String shift="Morning";
		if(j%2 != 0)
			shift="Evening";
		Schedule schedule=new Schedule();
		schedule.setDate(tempDate);
		schedule.setShift(shift);
		lstSch.add(schedule);
		schedule.setEmployeeName("Employee"+j);
	}
	
	for(int k=1;k<11;k++){
		SimpleDateFormat sdf=new SimpleDateFormat("dd-MMM-YYYY");
		cal.add(Calendar.DAY_OF_MONTH, -1);
		String tempDate=sdf.format(cal.getTime());
		String shift="Morning";
		if(k%2 == 0)
			shift="Evening";
		Schedule schedule=new Schedule();
		schedule.setDate(tempDate);
		schedule.setShift(shift);
		lstSch.add(schedule);
		schedule.setEmployeeName("Employee"+k);
	}

	return lstSch;
	}
}
